#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline author:t c:nil creator:comment d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t timestamp:t toc:nil todo:t |:t
#+TITLE: (Reusable) Post Calibrated Peer Review
#+AUTHOR: Jérémy Barbay
#+EMAIL: jeremy@barbay.cl
#+DESCRIPTION: 
#+KEYWORDS:
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.4.1 (Org mode 8.2.5h)
#+OPTIONS: texht:t
#+DATE: \today
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS:
#+LATEX_HEADER:
#+LATEX_HEADER_EXTRA: \usepackage{fullpage}

#+begin_abstract  
A web platform to facilitate the calibrated peer-reviewed correction of digital components of homeworks and exams:
Each evaluation consists in a description of the task to be performed by the students, a list of evaluation criteria, and eventually some positive and negative examples for each criteria.  The instructor creates a new evaluation or selects one already existing in the system, enters a deadline for submission and a deadline for evaluation. Students must upload their submission until the submission deadline, and must evaluate three submissions (among which can be hidden some of the positive and negative examples among submissions from their peers) before the evaluation deadline. The professor is then presented with a summary of the evaluation graph, and can pick up individual submissions in order to evaluate them, and refine the calibration of the peer-evaluation, until they are satisfied with the quality of the evaluation, at which point they can export the assignment and evaluation notes of the students.
#+end_abstract 

* Descriptions

The project can be described at various levels of details and to various public. 
You choose which description corresponds the best to your attention span and time available :)

** Elevator pitch:
(Reusable) Post Calibrated Peer Review: A web platform to facilitate the calibrated peer-reviewed correction of digital components of homeworks and exams.
** For Profesors

A web platform to report some of the evaluation to the students, so that they learn from other's mistakes.  Each evaluation consists in a description of the task to be performed by the students, a list of evaluation criteria, and eventually some positive and negative examples for each criteria.  The instructor creates a new evaluation or selects one already existing in the system, enters a deadline for submission and a deadline for evaluation. After the students did their bit, the professor is then presented with a summary of the evaluation graph, and can pick up individual submissions in order to evaluate them, and refine the calibration of the peer-evaluation, until they are satisfied with the quality of the evaluation, at which point they can export the assignment and evaluation notes of the students.

** For Students

A web platform where you grade your peers. Each task is assigned a deadline for submission and a deadline for evaluation. Students must upload their submission until the submission deadline, and must evaluate three submissions (among which submissions from their peers, but also fake submissions submitted by the instructor, some of them good and some of them bad) before the evaluation deadline. At the end of the process, each student is graded both on how well they did on the task, and how well they did at evaluating other's tasks.

** For Funding Agencies

A web platform to facilitate the calibrated peer-reviewed correction of digital components of homeworks and exams:
Each evaluation consists in a description of the task to be performed by the students, a list of evaluation criteria, and eventually some positive and negative examples for each criteria.  The instructor creates a new evaluation or selects one already existing in the system, enters a deadline for submission and a deadline for evaluation. Students must upload their submission until the submission deadline, and must evaluate three submissions (among which can be hidden some of the positive and negative examples among submissions from their peers) before the evaluation deadline. The professor is then presented with a summary of the evaluation graph, and can pick up individual submissions in order to evaluate them, and refine the calibration of the peer-evaluation, until they are satisfied with the quality of the evaluation, at which point they can export the assignment and evaluation notes of the students.


* Public
  - Instructors from all levels, in contexts with internet connectivity.
  - Students from all levels, in contexts with internet connectivity.


* Related Projects
  - [[ http://cpr.molsci.ucla.edu/Home ][Calibrated Peer Review]] :: "Calibrated Peer Review (CPR) is a web-based, instructional tool that enables frequent writing assignments in any discipline, with any class size, even in large classes with limited instructional resources. In fact, CPR can reduce the time an instructor now spends reading and assessing student writing.". The main difference is that PCPR is allowing to calibrate the evaluation AFTER the students have submitted their work. Another difference is the enfasis on "Fluid Collaboration", in the sense that each evaluation created by an instructor is by default made available to all other instructors, so that to promote collaboration across time and space.
  - [[https://www.teachingislearning.cl][Teaching is Learning / Video Clases]]  :: "El proyecto Teaching is Learning (TiL) es un servicio colaborativo de aprendizaje mediante el uso de material pedagógico realizado por los propios estudiantes. Los profesores pueden utilizar esta herramienta para promover el aprendizaje de los estudiantes, permitiendo la creatividad y al mismo tiempo aliviando la carga de las evaluaciones." A previous tentative, focused on Video Assignments, and not implementing neither pre calibrated peer review nor post calibrated peer review, nor reusability of evaluations.
* Propuestas
** Trabajo de Memoria DCC, 2020A
    * **Objetivo**: //(Reusable) Pre/Post Calibrated Peer Review//: A web platform to facilitate the calibrated peer reviewed correction of homeworks and exams. {{ :2016-logo-teachingislearning.png?nolink&100|}}
        * **Profesor Guía**: [[jeremy@barbay.cl|Jérémy Barbay]]
        * **Repository**: https://gitlab.com/GuidingLearning/pcpr
        * **Descripción**: A web platform to facilitate the calibrated peer-reviewed correction of digital components of homeworks and exams: Each evaluation consists in a description of the task to be performed by the students, a list of evaluation criteria, and eventually some positive and negative examples for each criteria.  The instructor creates a new evaluation or selects one already existing in the system, enters a deadline for submission and a deadline for evaluation. Students must upload their submission until the submission deadline, and must evaluate three submissions (among which can be hidden some of the positive and negative examples among submissions from their peers) before the evaluation deadline. The professor is then presented with a summary of the evaluation graph, and can pick up individual submissions in order to evaluate them, and refine the calibration of the peer-evaluation, until they are satisfied with the quality of the evaluation, at which point they can export the assignment and evaluation notes of the students. (//Proyecto válido como tema de Memoria o **Doble Titulación**//)

** Trabajo de Memoria DCC, 2019B

   * **Objetivo**: //Post-Calibrated Peer Review//: Un portal web para la evaluación por pares de tareas de alumnos. {{ :2016-logo-teachingislearning.png?nolink&100|}}
     * **Profesor Guía**: [[jeremy@barbay.cl|Jérémy Barbay]]
     * **Repository**: https://github.com/Videoclases/
     * **Descripción**:  En el proyecto http://teachingislearning.cl, alumnos de colegio y alumnos de la universidad profundizan su aprendizaje 1) desarrollando videos pedagógicos para sus pares, y 2) evaluando los videos pedagógicos de sus pares, usando el software "Video Clases", basado en un proceso de control de cualidad colectivo inspirado de "ReCaptcha". Se propone de 1) implementar a dentro del software "Video Clases" un modulo de "Post Calibrated Peer Review", que permite de mejorar el control del docente sobre la cualidad de la evaluación colectiva, y de 2) validar su calidad, en particular en cursos en linea, que sea en colegios o en universidad (e.g. profesor Patricio Moya de la Universidad Diego Portal).
* Milestones and Sub-Projects
** REVIEW existing software Video Clases
** ADD in the database scheme the email address of each student
** ADDITION of some of [0/4] basic features to Video Clases, as an exercize
   1. [ ] Editing the list of students of a class
   2. [ ] automatic generation of passwords for a class
   4. [ ] automatic parsing of various format of list of students
** ADD support for students to change their own password 
** ADD support for students to recover their account based on their email address
** EXTEND Video Classes to support any type of textual document or link to external document
** ADD support for pre-calibration
** ADD visualisation interface of the random evaluation graph 
** ADD interface for post-calibration by the instructor
** ADD module to export student notes to various Learning Management System (and to the Education Minister in Chile)
* Resources required
  - [ ] Django?
